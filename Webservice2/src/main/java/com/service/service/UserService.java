package com.service.service;

import com.service.entity.User;

public interface UserService {

	User createUser(User user);
}
