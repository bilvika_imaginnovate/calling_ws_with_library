package com.service.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.service.entity.User;
import com.service.repo.UserRepo;

@Service
public class UserServiceImpl implements UserService{
	
@Autowired
UserRepo userRepo;

public User createUser(User A) {
	return userRepo.save(A);
}
}


