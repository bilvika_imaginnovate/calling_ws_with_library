package com.ws2.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws2.client.UserOperationsClient;

@RestController
@RequestMapping("/client")
public class UserClientController {
	@Autowired
	UserOperationsClient userOperationsClient;
	

	@PostMapping("/addUser")
	public User createUser(@RequestBody User user) {
		return userOperationsClient.createuser(user);


}
}

