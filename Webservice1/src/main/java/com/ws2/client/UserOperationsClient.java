package com.ws2.client;

import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.stereotype.Service;
import com.ws2.UserDto.UserClientController;


@Service
public class UserOperationsClient {
	
	UserClient userClient  = new UserClient();
	public User createuser(User user) {
		return userClient.createUser(user);
	
	}
}