package com.lib.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibUserServiceApplication.class, args);
	}

}
