package com.lib.UserClient_Dto;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UserRestTemp{
	
	public UserRestTemp createUser(UserRestTemp user) {
		RestTemplate template = new RestTemplate();
		RequestEntity<UserRestTemp> request = null;
		try {
			request = RequestEntity
				     .post(new URI("http://localhost:8081/user/create"))
				     .accept(MediaType.APPLICATION_JSON)
				     .body(user);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		ResponseEntity<UserRestTemp> response = template.exchange(request, UserRestTemp.class);

		return response.getBody();
	}

}